#include <stdlib.h>
#include <stdio.h>
#include <gb/gb.h>

#include "globals.h"

#include "font_helpers.h"

#include "tileset_town.h"
#include "tilemap_town_0.h"

#include "spritesheet.h"

#define INTERACTING_DELAY 32
UINT8 INTERACTING_FRAME_SKIP;

#define PLAYER_SPRITESHEET_DATA_OFFSET 0

// Player' sprite Ids.
#define PLAYER_SPRITE_L_ID 0
#define PLAYER_SPRITE_R_ID 1

#define PLAYER_ANIM_SPEED 8

UINT8 PLAYER_ANIM_FRAME;
UINT8 PLAYER_ANIM_DIRECTION;
UINT8 PLAYER_ANIM_OFFSET;

UINT8 FRAME_SKIP;

// Animation data for both side.
UINT8 PLAYER_SPRITE_ANIM[] = {
// SIZE | MIRRORED | TILES ID 		| DIRECTION
   4,       0,        4 , 8,  4,  0,	// Down
   4,       0,       16, 20, 16, 12,	// Up
   2,       0,       24, 28,			// Left
   2,       0,       32, 36,			// Right
};

#define PLAYER_SPRITE_ANIM_LEFT 12
#define PLAYER_SPRITE_ANIM_RIGHT 16
#define PLAYER_SPRITE_ANIM_UP 6
#define PLAYER_SPRITE_ANIM_DOWN 0

#define PLAYER_MOVING_ANIM_SIZE 40

#define PLAYER_WALKING_ANIM_OFFSET 0
#define PLAYER_BIKING_ANIM_OFFSET (1 * PLAYER_MOVING_ANIM_SIZE)

#define WORLD_STATE_ROAMING 0
#define WORLD_STATE_TALKING 1

UINT8 WORLD_STATE;

UINT8 WORLDSCENE_MAP_PART_X;
UINT8 WORLDSCENE_MAP_PART_Y;

UINT16 TILESET_TOWN_WALKABLE_MAP[] = {
	0x0009,
	0x0000,
	0x1000,
	0x0200,
	0x0000,
	0x0804,
};

#define DIALOG_BOX_LINE_COUNT 5

UINT8 SIGN_TO_READ;
UINT8 SIGN_CURRENT_SENTENCE;
UINT8 SIGN_DISPLAY_OFFSET;

struct SignData
{
	UINT8 mapPos[2];
	UINT8 bkgPos[2];
	UINT8 sentenceCount;
	UINT8 firstSentenceOffset;
};

#define SIGN_COUNT 2
const struct SignData SIGN_DATA[] = {
	{{0, 0}, {4, 12}, 2, 0},
	{{1, 0}, {11, 5}, 1, 2}
};

#define BOTTOM_LEFT_SIGN_TILE 0x56
#define BOTTOM_RIGHT_SIGN_TILE 0x57

const unsigned char SIGN_0_SENTENCE_0[] = "MAISON DE LUKA\nYEAH !";
const unsigned char SIGN_0_SENTENCE_1[] = "BIENVENUE !";
const unsigned char SIGN_1_SENTENCE_0[] = "MAISON DE MARGOT";
const unsigned char* SIGN_SENTENCES[] = {
	SIGN_0_SENTENCE_0, // SIGN 0
	SIGN_0_SENTENCE_1,
	SIGN_1_SENTENCE_0 // SIGN 1
};

INT8 FindSign(UINT8 leftBkgTileX, UINT8 upperLeftBkgTileY)
{
	for (UINT8 i = 0; i < SIGN_COUNT; ++i)
	{
		UINT8 signPositionX = SIGN_DATA[i].mapPos[0];
		UINT8 signPositionY = SIGN_DATA[i].mapPos[1];
		if (WORLDSCENE_MAP_PART_X == signPositionX && WORLDSCENE_MAP_PART_Y == signPositionY)
		{	
			if (upperLeftBkgTileY == SIGN_DATA[i].bkgPos[1])
			{
				UINT8 signLeftBackgroundPositionX = SIGN_DATA[i].bkgPos[0];
				UINT8 signRightBackgroundPositionX = signLeftBackgroundPositionX + 1;
				if (leftBkgTileX == signLeftBackgroundPositionX || leftBkgTileX == signRightBackgroundPositionX)
				{
					return i;
				}

				UINT8 upperRightTileX = leftBkgTileX + 1;
				if (upperRightTileX == signLeftBackgroundPositionX || upperRightTileX == signRightBackgroundPositionX)
				{
					return i;
				}
			}
		}
	}

	return -1;
}

void DisplaySignCurrentSentence()
{
	// Create empty line.
	UINT8 emptyLine[SCREEN_TILE_WIDTH];
	for (UINT8 i = 0; i < SCREEN_TILE_WIDTH; ++i)
	{
		emptyLine[i] = 0;
	}

	// Clear screen regarding where to display the text.
	for (UINT8 i = 0; i < DIALOG_BOX_LINE_COUNT; ++i)
	{
		set_bkg_tiles(0, SIGN_DISPLAY_OFFSET + i, SCREEN_TILE_WIDTH, 1, emptyLine);
	}

	// Get sentence to display.
	UINT8 sentence = SIGN_DATA[SIGN_TO_READ].firstSentenceOffset + SIGN_CURRENT_SENTENCE;
	const unsigned char* signSentence = SIGN_SENTENCES[sentence];
	// Display the sentence.
	text_print_string(1, SIGN_DISPLAY_OFFSET + 1, 0, signSentence);	
}

void LoadMapPart(UINT8 mapPartX, UINT8 mapPartY)
{
    for (UINT8 i = 0; i < SCREEN_TILE_HEIGHT; ++i)
    {
    	const UINT8* tiles = TILEMAP_TOWN_0 + (mapPartX * SCREEN_TILE_WIDTH) + (mapPartY * SCREEN_TILE_HEIGHT + i) * TILEMAP_TOWN_0_WIDTH;
    	set_bkg_tiles(0, i, SCREEN_TILE_WIDTH, 1, tiles);
    }	
}

UINT8 IsTileWalkable(UINT8 x, UINT8 y)
{
	UINT8 tile;
	get_bkg_tiles(x, y, 1, 1, &tile);

	UINT8 line = DIV16(tile);

	UINT16 tileRelativeWalkableMap = TILESET_TOWN_WALKABLE_MAP[line];

	UINT8 mapValue = MOD16(tile);

	UINT16 mask = 1 << mapValue;

	UINT16 isWalkable = tileRelativeWalkableMap & mask;

	return isWalkable > 0;
}

void WorldScene_Setup()
{
	// DATA
	INTERACTING_FRAME_SKIP = 0;

	WORLDSCENE_MAP_PART_X = 0;
	WORLDSCENE_MAP_PART_Y = 0;

	SCREEN_X = 4 * 8;
	SCREEN_Y = 14 * 8;

	// BACKGROUND

	// Load tilset town.
    set_bkg_data(0, TILESET_TOWN_TILE_COUNT, TILESET_TOWN);
    // Load tilemap town.
    LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);
    /*for (UINT8 i = 0; i < 18; ++i)
    {
    	set_bkg_tiles(0, i, 20, 1, TILEMAP_TOWN_0 + (WORLDSCENE_MAP_PART_X * 20) + (WORLDSCENE_MAP_PART_Y * 18 + i) * TILEMAP_TOWN_0_WIDTH);
    }*/

	// Set background visible.
	SHOW_BKG;

	// SPRITES

	PLAYER_ANIM_OFFSET = PLAYER_BIKING_ANIM_OFFSET;	

	// Load spritesheet.
	set_sprite_data(PLAYER_SPRITESHEET_DATA_OFFSET, SPRITESHEET_TILE_COUNT, SPRITESHEET);

	// Set sprite PLAYER_SPRITE_L_ID with tile 0.
	set_sprite_tile(PLAYER_SPRITE_L_ID, 4);
	// Set sprite PLAYER_SPRITE_R_ID with tile 2.
	set_sprite_tile(PLAYER_SPRITE_R_ID, 6);

	// Set sprites visible.
	SHOW_SPRITES;
}

void WorldSceneStateRoaming()
{
	KEYS = joypad();
	UINT8 isWalking = 1;
	if (KEYS & J_LEFT)
	{
		PLAYER_ANIM_DIRECTION = PLAYER_SPRITE_ANIM_LEFT;

		// On left limit.
		if (SCREEN_X == 0)
		{
			// Load map part to the left.
			WORLDSCENE_MAP_PART_X -= 1;
			LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);
			SCREEN_X = SCREENWIDTH - SPRITE_WIDTH;
		}
		else
		{
			const UINT8 potentialBackgroundTileX = WORLD_2_BACKGROUND_TILE_X(SCREEN_X - 1);
			// If not colliding.
			if (IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y))
				&& IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y + SPRITE_HEIGHT / 2))
				&& IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y + SPRITE_HEIGHT - 1))
				)
			{
				// Go left.
				SCREEN_X -= 1;
			}	
		}
	}
	else if (KEYS & J_RIGHT)
	{
		PLAYER_ANIM_DIRECTION = PLAYER_SPRITE_ANIM_RIGHT;

		// On right limit.
		if (SCREEN_X == SCREENWIDTH - SPRITE_WIDTH)
		{
			// Load map part to the right.
			WORLDSCENE_MAP_PART_X += 1;
			LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);
			SCREEN_X = 0;
		}
		else
		{
			// If not colliding.
			const UINT8 potentialBackgroundTileX = WORLD_2_BACKGROUND_TILE_X(SCREEN_X + SPRITE_WIDTH);
			if (IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y))
				&& IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y + SPRITE_HEIGHT / 2))
				&& IsTileWalkable(potentialBackgroundTileX, WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y + SPRITE_HEIGHT - 1))
				)
			{
				// Go right.
				SCREEN_X += 1;
			}	
		}
	}
	else if (KEYS & J_UP)
	{
		PLAYER_ANIM_DIRECTION = PLAYER_SPRITE_ANIM_UP;

		// On top limit.
		if (SCREEN_Y == 0)
		{
			// Load map part above.
			WORLDSCENE_MAP_PART_Y -= 1;
			LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);
			SCREEN_Y = SCREENHEIGHT - SPRITE_HEIGHT;
		}
		else
		{
			const UINT8 potentialBackgroundTileY = WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y - 1);
			// If not colliding.
			if (IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X), potentialBackgroundTileY)
				&& IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X + SPRITE_WIDTH / 2), potentialBackgroundTileY)
				&& IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X + SPRITE_WIDTH - 1), potentialBackgroundTileY)
				)
			{
				// Go up.
				SCREEN_Y -= 1;
			}
		}
	}
	else if (KEYS & J_DOWN)
	{
		PLAYER_ANIM_DIRECTION = PLAYER_SPRITE_ANIM_DOWN;

		// On bottom limit.
		if (SCREEN_Y + SPRITE_HEIGHT == SCREENHEIGHT)
		{
			// Load map part below.
			WORLDSCENE_MAP_PART_Y += 1;
			LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);
			SCREEN_Y = 0;
		}
		else
		{
			const UINT8 potentialBackgroundTileY = WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y + SPRITE_HEIGHT);
			// If not colliding.
			if (IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X), potentialBackgroundTileY)
				&& IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X + SPRITE_WIDTH / 2), potentialBackgroundTileY)
				&& IsTileWalkable(WORLD_2_BACKGROUND_TILE_X(SCREEN_X + SPRITE_WIDTH - 1), potentialBackgroundTileY)
				)
			{
				// Go down.
				SCREEN_Y += 1;
			}
		}
	}
	else
	{
		if (INTERACTING_FRAME_SKIP == INTERACTING_DELAY)
		{
			if (KEYS & J_A)
			{
				const UINT8 leftBkgTileX = WORLD_2_BACKGROUND_TILE_X(SCREEN_X);
				const UINT8 upperLeftBkgTileY = WORLD_2_BACKGROUND_TILE_Y(SCREEN_Y - 1);

				INT8 foundSign = FindSign(leftBkgTileX, upperLeftBkgTileY);
				if (foundSign != -1)
				{
					SIGN_TO_READ = foundSign;

					// Compute where to display the text.
					if (SCREEN_Y < 8 * 8)
					{
						SIGN_DISPLAY_OFFSET = 13;
					}
					else
					{
						SIGN_DISPLAY_OFFSET = 0;
					}

					SIGN_CURRENT_SENTENCE = 0;

					DisplaySignCurrentSentence();

					INTERACTING_FRAME_SKIP = 0;

					WORLD_STATE = WORLD_STATE_TALKING;
				}
			}
		}

		isWalking = 0;
		FRAME_SKIP = PLAYER_ANIM_SPEED - 1;
	}

	if (isWalking)
	{
		++FRAME_SKIP;
		if (FRAME_SKIP == PLAYER_ANIM_SPEED)
		{
			FRAME_SKIP = 0;

			PLAYER_ANIM_FRAME = (PLAYER_ANIM_FRAME + 1) % PLAYER_SPRITE_ANIM[PLAYER_ANIM_DIRECTION];
		}	
	}
	else
	{
		PLAYER_ANIM_FRAME = 0;
	}

	const UINT8 tile = PLAYER_ANIM_OFFSET + PLAYER_SPRITE_ANIM[PLAYER_ANIM_DIRECTION + PLAYER_ANIM_FRAME + 2];

	// Set sprite PLAYER_SPRITE_L_ID with tile 0.
	set_sprite_tile(PLAYER_SPRITE_L_ID, tile);
	// Set sprite PLAYER_SPRITE_R_ID with tile 2.
	set_sprite_tile(PLAYER_SPRITE_R_ID, tile + 2);	

	move_sprite(PLAYER_SPRITE_L_ID, SCREEN_X + 8, SCREEN_Y + 16);
	move_sprite(PLAYER_SPRITE_R_ID, SCREEN_X + 8 + 8, SCREEN_Y + 16);			

	if (INTERACTING_FRAME_SKIP != INTERACTING_DELAY)
	{
		++INTERACTING_FRAME_SKIP;
	}
}

void WorldSceneStateTalking()
{
	if (INTERACTING_FRAME_SKIP == INTERACTING_DELAY)
	{
		KEYS = joypad();
		if (KEYS & J_A)
		{
			UINT8 signSentenceCount = SIGN_DATA[SIGN_TO_READ].sentenceCount;
			if (SIGN_CURRENT_SENTENCE < signSentenceCount - 1)
			{
				++SIGN_CURRENT_SENTENCE;

				DisplaySignCurrentSentence();
			}
			else
			{
				LoadMapPart(WORLDSCENE_MAP_PART_X, WORLDSCENE_MAP_PART_Y);

				WORLD_STATE = WORLD_STATE_ROAMING;
			}

			INTERACTING_FRAME_SKIP = 0;
		}	
	}

	if (INTERACTING_FRAME_SKIP != INTERACTING_DELAY)
	{
		++INTERACTING_FRAME_SKIP;
	}
}

void WorldScene_Loop()
{
	// Wait screen refresh (v-sync).
	wait_vbl_done();

	if (WORLD_STATE == WORLD_STATE_ROAMING)
	{
		WorldSceneStateRoaming();
	}
	else if (WORLD_STATE == WORLD_STATE_TALKING)
	{
		WorldSceneStateTalking();
	}
}

// Title
void WorldScene()
{
	/*sprintf(DEBUG_LINE, "(%d)", SCREEN_Y);
	text_print_string(10, 16, 0, DEBUG_LINE);*/

	/*sprintf(DEBUG_LINE, "(%d)", WORLD_Y);
	text_print_string(10, 17, 0, DEBUG_LINE);*/

	WorldScene_Setup();

	while(1)
	{
		WorldScene_Loop();
	}
}

/****************
*****************
****************/

void Setup()
{
	// BACKGROUND

	// Load background's font tileset.
	text_load_font();

	// SPRITES

	// Set default palette to (white, white, light grey, black).
	OBP0_REG = 0xD0;

	// Use 8x18 sprites.
	SPRITES_8x16;
}

void Loop()
{
	WorldScene();
}

void main(void)
{
	Setup();
	while(1)
	{
		Loop();
	}
}

