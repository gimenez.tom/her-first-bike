#include "font_helpers.h"

#include <gb/gb.h>

#include "./font_tileset.h"

void text_load_font()
{
    set_bkg_data(TEXT_FONT_OFFSET, FONT_TILESET_TILE_COUNT, FONT_TILESET);
}

// Write the given char at the (x, y) position on the Background (0) or Window (1) layer
void text_print_char(UINT8 x, UINT8 y, UINT8 layer, unsigned char chr)
{
    UINT8 tile;

    if (chr >= 'a' && chr <= 'z') {
        tile = _TEXT_CHAR_A + chr - 'a';
    } else if (chr >= 'A' && chr <= 'Z') {
        tile = _TEXT_CHAR_A + chr - 'A';
    } else if (chr >= '0' && chr <= '9') {
        tile = _TEXT_CHAR_0 + chr - '0';
    } else {
        switch (chr) {
            case '\'':
                tile = (UINT8)_TEXT_CHAR_APOSTROPHE;
                break;
            case '-':
                tile = (UINT8)_TEXT_CHAR_DASH;
                break;
            case '.':
                tile = (UINT8)_TEXT_CHAR_DOT;
                break;
            case ',':
                tile = (UINT8)_TEXT_CHAR_COMMA;
                break;
            case ':':
                tile = (UINT8)_TEXT_CHAR_COLON;
                break;
            case '!':
                tile = (UINT8)_TEXT_CHAR_EXCLAMATION;
                break;
            case '?':
                tile = (UINT8)_TEXT_CHAR_INTERROGATION;
                break;
            case '(':
                tile = (UINT8)_TEXT_CHAR_LPARENTHESES;
                break;
            case ')':
                tile = (UINT8)_TEXT_CHAR_RPARENTHESES;
                break;
            case ' ':
                tile = (UINT8)_TEXT_CHAR_SPACE;
                break;
            default:
                tile = (UINT8)_TEXT_CHAR_TOFU;
        }
    }

    if (layer == 0)
    {
        set_bkg_tiles(x, y, 1, 1, &tile);
    }
    else
    {
        set_win_tiles(x, y, 1, 1, &tile);
    }
}

// Write the given string at the (x, y) position on the Background (0) or Window (1) layer
// Line feed (\n) allowed
void text_print_string(UINT8 x, UINT8 y, UINT8 layer, const unsigned char *string)
{
    UINT8 offset_x = 0;
    UINT8 offset_y = 0;

    while (string[0]) {
        if (string[0] == '\n') {
            offset_x = 0;
            offset_y += 1;
        } else {
            text_print_char(x + offset_x, y + offset_y, layer, (unsigned char) string[0]);
            offset_x += 1;
        }

        string += 1;  // Increment pointer address, /!\ THIS IS DANGEROUS!
    }
}
