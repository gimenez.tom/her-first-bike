#ifndef _GLOBALS_H
#define _GLOBALS_H

#include "types.h"

#define DIV8(v) ((v) >> 3) // v / 8
#define DIV16(v) ((v) >> 4) // v / 16
#define MOD8(v) ((v) & 0x07) // v % 8
#define MOD16(v) ((v) & 0x0F) // v % 16
#define MOD32(v) ((v) & 0x1F) // v % 32
#define WORLD_2_BACKGROUND_TILE_X(x) (MOD32(DIV8(x)))
#define WORLD_2_BACKGROUND_TILE_Y(y) (MOD32(DIV8(y)))
#define WORLD_2_SCREEN_X(x) (x % (20 * 8))
#define WORLD_2_SCREEN_Y(y) (y % (18 * 8))

#define SCREEN_TILE_WIDTH 20U
#define SCREEN_TILE_HEIGHT 18U

#define SPRITE_WIDTH 16U
#define SPRITE_HEIGHT 16U

extern UINT8 SCREEN_X;
extern UINT8 SCREEN_Y;

extern UINT8 WORLD_X;
extern UINT8 WORLD_Y;

extern UINT16 LEFT;
extern UINT16 TOP;

extern UINT8 KEYS;

extern char DEBUG_LINE[10];

extern const UINT8* CURRENT_TILEMAP;

#endif //_GLOBALS_H