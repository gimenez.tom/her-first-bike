#!/bin/sh

img2gb tileset \
	--output-c-file=../src/spritesheet.c \
    --output-header-file=../src/spritesheet.h \
    --output-image=spritesheet_output.png \
    --sprite8x16 \
    --name=SPRITESHEET \
    ./spritesheet.png