<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="town" tilewidth="8" tileheight="8" tilecount="96" columns="16" objectalignment="topleft">
 <image source="tileset_town.png" width="128" height="48"/>
 <tile id="0" type="walkable"/>
 <tile id="1" type="walkable"/>
 <tile id="3" type="walkable"/>
 <tile id="13" type="walkable"/>
 <tile id="57" type="walkable"/>
 <tile id="82" type="walkable"/>
</tileset>
