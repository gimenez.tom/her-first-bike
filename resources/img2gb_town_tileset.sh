#!/bin/sh

img2gb tileset \
	--output-c-file=../src/tileset_town.c \
    --output-header-file=../src/tileset_town.h \
    --output-image=tileset_town_output.png \
    ./tileset_town.png